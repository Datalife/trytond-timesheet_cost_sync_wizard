# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .timesheet import TimesheetSyncCost, TimesheetSyncCostStart


def register():
    Pool.register(
        TimesheetSyncCostStart,
        module='timesheet_cost_sync_wizard', type_='model')
    Pool.register(
        TimesheetSyncCost,
        module='timesheet_cost_sync_wizard', type_='wizard')
