# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView
from trytond.pool import Pool
from trytond.model import fields
from trytond.wizard import Button, StateTransition, StateView, Wizard
from trytond.transaction import Transaction

__all__ = ['TimesheetSyncCostStart', 'TimesheetSyncCost']


class TimesheetSyncCostStart(ModelView):
    """Timesheet Sync cost start"""
    __name__ = 'timesheet.line.sync_cost.start'

    start_date = fields.Date('Start date', required=True)
    end_date = fields.Date('End date')
    work = fields.Many2One('timesheet.work', 'Work')
    employee = fields.Many2One('company.employee', 'Employee')
    costless = fields.Boolean('Only costless',
        help='Will compute cost only for lines without it.')
    companies = fields.Many2Many('company.company', None, None, 'Companies')

    @staticmethod
    def default_companies():
        if Transaction().context.get('company'):
            return [Transaction().context.get('company')]

    @staticmethod
    def default_start_date():
        Date_ = Pool().get('ir.date')
        return Date_.today()

    @staticmethod
    def default_costless():
        return True


class TimesheetSyncCost(Wizard):
    """Timesheet Sync cost"""
    __name__ = 'timesheet.line.sync_cost'

    start = StateView('timesheet.line.sync_cost.start',
        'timesheet_cost_sync_wizard.line_sync_cost_start_view_form',
        [Button('Cancel', 'end', 'tryton-cancel'),
         Button('OK', 'sync_', 'tryton-ok', default=True)])
    sync_ = StateTransition()

    def transition_sync_(self):
        pool = Pool()
        Line = pool.get('timesheet.line')

        _domain = [
            ('company', 'in', self.start.companies),
            ('date', '>=', self.start.start_date),
        ]
        if getattr(self.start, 'end_date', None):
            _domain.append(('date', '<=', self.start.end_date))
        if getattr(self.start, 'work', None):
            _domain.append(('work', '=', self.start.work.id))
        if getattr(self.start, 'employee', None):
            _domain.append(('employee', '=', self.start.employee.id))
        if self.start.costless:
            _domain.append(('cost_price', '=', 0))

        lines = Line.search(_domain)
        if lines:
            Line.sync_cost(lines)
        return 'end'
