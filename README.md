datalife_timesheet_cost_sync_wizard
===================================

The timesheet_cost_sync_wizard module of the Tryton application platform.

[![Build Status](http://drone.datalifeit.es:8050/api/badges/datalifeit/trytond-timesheet_cost_sync_wizard/status.svg)](http://drone.datalifeit.es:8050/datalifeit/trytond-timesheet_cost_sync_wizard)

Installing
----------

See INSTALL


License
-------

See LICENSE

Copyright
---------

See COPYRIGHT
