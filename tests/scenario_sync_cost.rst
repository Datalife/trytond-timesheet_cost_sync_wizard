======================
Timesheet sync cost
=======================

Imports::

    >>> import datetime
    >>> from decimal import Decimal
    >>> from trytond.tests.tools import activate_modules
    >>> from dateutil.relativedelta import relativedelta
    >>> from trytond.modules.company.tests.tools import create_company, get_company
    >>> from proteus import Model, Wizard
    >>> today = datetime.date.today()

Install Module::

    >>> config = activate_modules('timesheet_cost_sync_wizard')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create timesheet lines::

    >>> Employee = Model.get('company.employee')
    >>> Party = Model.get('party.party')
    >>> Work = Model.get('timesheet.work')
    >>> Line = Model.get('timesheet.line')
    >>> party = Party(name='Empleado 1')
    >>> party.save()
    >>> employee = Employee(company=company, party=party)
    >>> employee.save()
    >>> work = Work(name='Work 1')
    >>> work.save()
    >>> line = Line(employee=employee, date=today, work=work, duration=datetime.timedelta(hours=8))
    >>> line.save()
    >>> line.cost_price
    Decimal('0')

Create employee cost prices::

    >>> Party = Model.get('party.party')
    >>> Employee = Model.get('company.employee')
    >>> EmployeeCostPrice = Model.get('company.employee_cost_price')
    >>> EmployeeCostPrice(employee=employee, date=today,
    ...     cost_price=Decimal(10)).save()
    >>> line.reload()
    >>> line.cost_price
    Decimal('0')

Sync cost::

    >>> sync_ = Wizard('timesheet.line.sync_cost', [])
    >>> sync_.form.start_date = today
    >>> sync_.execute('sync_')
    >>> line.reload()
    >>> line.cost_price
    Decimal('10')
