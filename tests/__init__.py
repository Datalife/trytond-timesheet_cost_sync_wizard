# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_timesheet_cost_sync_wizard import suite

__all__ = ['suite']
